const accordionItemHeaders = document.querySelectorAll(".accordion-item-header");

accordionItemHeaders.forEach(accordionItemHeader => {
  accordionItemHeader.addEventListener("click", event => {
    accordionItemHeader.classList.toggle("active");
    const accordionItemBody = accordionItemHeader.nextElementSibling;
    if(accordionItemHeader.classList.contains("active")) {
      accordionItemBody.style.maxHeight = accordionItemBody.scrollHeight + "px";
    }
    else {
      accordionItemBody.style.maxHeight = 0;
    }
    
  });
});

const navSlide = () => {
    const burger = document.querySelector('.burger');
    const nav = document.querySelector('.nav-links');
    const navLinks  = document.querySelectorAll('.nav-links li')

    
    burger.addEventListener('click',() =>{
        //toggle nav
        nav.classList.toggle('nav-active');
        
        // Animate Links
        navLinks.forEach((link, index) => {
            if (link.style.animation) {
                link.style.animation = ''
            } else {
                link.style.animation = `navLinkFade 0.5s ease forwards ${index / 5 + 0.5}s`;
            }
        });
        //burger animation
        burger.classList.toggle('toggle');
    });
    //animate links
    
}

navSlide();

// $(document).ready(function () {
//   $('.accordion').accordion({
//       collapsible: true,
//       active: false,
//       height: 'fill',
//       header: 'h3'
//   }).sortable({
//       items: '.s_panel'
//   });

//   $('.accordion').on('accordionactivate', function (event, ui) {
//         if (ui.newPanel.length) {
//           $('.accordion').sortable('disable');
//       } else {
//           $('.accordion').sortable('enable');
//       }
//   });
// });

// $(function() {
//   $('.up').on('click', function(e) {
//     var wrapper = $(this).closest('.accordion-item-header')
//     wrapper.insertBefore(wrapper.prev())
//   })
//   $('.down').on('click', function(e) {
//     var wrapper = $(this).closest('.accordion-item-header')
//     wrapper.insertAfter(wrapper.next())
//   })
// })
