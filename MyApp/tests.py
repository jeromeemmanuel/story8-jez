from django.test import TestCase,Client,LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
import unittest
import time
import datetime


# Create your tests here.

class Story8UnitTest(TestCase):
    
    def test_story_8_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story_8_using_hello_world_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'MyApp/index.html')

    def test_story_7_landing_page_contains_hello_world(self):
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn("Hello",html_response)

# class TestProject(StaticLiveServerTestCase):
#     def setUp(self):
#         chrome_options = webdriver.ChromeOptions()
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--disable-dev-shm-usage')
#         self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
    
#     def tearDown(self):
#         self.browser.quit()
    
#     def funct_test(self):
#         self.browser.get('http://story8-jerome.herokuapp.com')
#         response_content = self.browser.page_source

#         self.assertIn("Accordion", response_content)

#         self.browser.find_element_by_css_selector('#activities > a').click()
#         time.sleep(1)

#         self.assertIn("Who", response_content)

class Story7FunctionalTest(TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()

    def funct_test(self):
        self.browser.get('http://story8-jerome.herokuapp.com/')
        response_content = self.browser.page_source

        self.assertIn("Accordion", response_content)
        self.browser.find_element_by_css_selector('#activities > a').click()
        time.sleep(1)

        self.assertIn("Who", response_content)

if __name__ == '__main__': 
    unittest.main(warnings='ignore') #
